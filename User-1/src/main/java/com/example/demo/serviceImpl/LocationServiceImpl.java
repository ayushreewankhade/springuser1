package com.example.demo.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.UserDto;
import com.example.demo.dto.UserLocationDto;
import com.example.demo.entity.Location;
import com.example.demo.repository.LocationRepo;
import com.example.demo.service.LocationService;

@Service
public class LocationServiceImpl implements LocationService {

	@Autowired
	private LocationRepo locationRepo;
	
	public List<UserLocationDto> getAllUserLocation(){
		return locationRepo.findAll()
				.stream()
				.map(this::convertEntityToDto)
				.collect(Collectors.toList());
	}
	
	private UserLocationDto convertEntityToDto(Location location) {
		UserLocationDto userLocationDto= new UserLocationDto();
		userLocationDto.setUserId(location.getId());
		userLocationDto.setPlace(location.getPlace());
		userLocationDto.setDescription(location.getDescription());
		return userLocationDto;
	}

	
}
