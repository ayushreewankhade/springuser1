package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.UserDto;
import com.example.demo.dto.UserLocationDto;
import com.example.demo.service.LocationService;
import com.example.demo.service.UserService;

@RestController
public class UserLocationController {

	@Autowired
	private LocationService locationService;
	
	@GetMapping("/user-location")
	public List<UserLocationDto> getAllUserLocation(){
		return locationService.getAllUserLocation();
	}
	
	
	
}
