package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Location {


	@Id
	private long id;
	
	private String place;
	private String description;
	
	@OneToOne(mappedBy= "location")
	private User user;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Location(long id, String place, String description) {
		super();
		this.id = id;
		this.place = place;
		this.description = description;
	}
	public Location() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Location [id=" + id + ", place=" + place + ", description=" + description + "]";
	}
	
	
}
